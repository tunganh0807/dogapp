import 'package:dog_app/network/api_client.dart';
import 'package:dog_app/network/api_util.dart';
import 'package:dog_app/repository/breed_repository.dart';
import 'package:dog_app/repository/image_repository.dart';
import 'package:dog_app/ui/splash/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ApiClient _apiClient;

  @override
  void initState() {
    super.initState();
    _apiClient = ApiUtil.apiClient;
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<BreedRepository>(create: (context) {
          return BreedRepositoryImpl(apiClient: _apiClient);
        }),
        RepositoryProvider<ImageRepository>(create: (context) {
          return ImageRepositoryImpl(apiClient: _apiClient);
        }),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashPage(),
      ),
    );
  }
}
