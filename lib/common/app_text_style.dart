import 'package:flutter/material.dart';

class AppTextStyle {
  static TextStyle black = const TextStyle(color: Colors.black);
  static TextStyle white = const TextStyle(color: Colors.white);

  static TextStyle blackS32 = black.copyWith(fontSize: 32);
  static TextStyle blackS20 = black.copyWith(fontSize: 20);
}
