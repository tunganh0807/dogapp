import 'package:dog_app/model/entity/image_by_breed_entity/image_by_breed_entity.dart';
import 'package:dog_app/network/api_client.dart';

abstract class ImageRepository {
  Future<ImageByBreedEntity?> getImageEntity({required String breed});
}

class ImageRepositoryImpl extends ImageRepository {
  ApiClient apiClient;

  ImageRepositoryImpl({
    required this.apiClient,
  });

  @override
  Future<ImageByBreedEntity?> getImageEntity({required String breed}) {
    return apiClient.getImageByBreed(breed);
  }
}
