import 'package:dog_app/model/entity/breeds_entity/breeds_entity.dart';
import 'package:dog_app/network/api_client.dart';

abstract class BreedRepository {
  Future<BreedsEntity?> getBreedEntity();
}

class BreedRepositoryImpl extends BreedRepository {
  ApiClient apiClient;

  BreedRepositoryImpl({
    required this.apiClient,
  });

  @override
  Future<BreedsEntity?> getBreedEntity() {
    return apiClient.getBreedsEntity();
  }
}
