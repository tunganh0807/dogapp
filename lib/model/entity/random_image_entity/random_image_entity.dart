import 'package:json_annotation/json_annotation.dart';

part 'random_image_entity.g.dart';

@JsonSerializable()
class RandomImageEntity {
  @JsonKey(name: 'status')
  final String? status;

  @JsonKey(name: 'message')
  final String? imagePath;

  RandomImageEntity({
    this.status,
    this.imagePath,
  });

  factory RandomImageEntity.fromJson(Map<String, dynamic> json) =>
      _$RandomImageEntityFromJson(json);

  Map<String, dynamic> toJson() => _$RandomImageEntityToJson(this);
}
