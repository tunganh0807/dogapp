// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'random_image_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RandomImageEntity _$RandomImageEntityFromJson(Map<String, dynamic> json) =>
    RandomImageEntity(
      status: json['status'] as String?,
      imagePath: json['message'] as String?,
    );

Map<String, dynamic> _$RandomImageEntityToJson(RandomImageEntity instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.imagePath,
    };
