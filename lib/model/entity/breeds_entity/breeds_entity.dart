import 'package:dog_app/model/entity/breeds_entity/breeds_custom_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'breeds_entity.g.dart';

@JsonSerializable()
class BreedsEntity {
  @JsonKey(name: 'status')
  final String? status;

  @JsonKey(name: 'message')
  final Map<String, List<String>>? message;

  BreedsEntity({
    this.status,
    this.message,
  });

  List<BreedsCustomEntity> getListBreed() {
    List<BreedsCustomEntity> listBreed = [];
    List<String> list = message?.keys.toList() ?? [];
    for (int i = 0; i < list.length; i++) {
      listBreed.add(
        BreedsCustomEntity(
          breed: list[i],
        ),
      );
    }
    return listBreed;
  }

  factory BreedsEntity.fromJson(Map<String, dynamic> json) =>
      _$BreedsEntityFromJson(json);

  Map<String, dynamic> toJson() => _$BreedsEntityToJson(this);
}
