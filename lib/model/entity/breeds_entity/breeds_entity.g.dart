// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'breeds_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BreedsEntity _$BreedsEntityFromJson(Map<String, dynamic> json) => BreedsEntity(
      status: json['status'] as String?,
      message: (json['message'] as Map<String, dynamic>?)?.map(
        (k, e) =>
            MapEntry(k, (e as List<dynamic>).map((e) => e as String).toList()),
      ),
    );

Map<String, dynamic> _$BreedsEntityToJson(BreedsEntity instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
    };
