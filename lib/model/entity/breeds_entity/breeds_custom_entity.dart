class BreedsCustomEntity {
  late String breed;
  late bool isSelect;

  BreedsCustomEntity({
    required this.breed,
    this.isSelect = false,
  });
}
