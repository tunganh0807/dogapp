// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_by_breed_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageByBreedEntity _$ImageByBreedEntityFromJson(Map<String, dynamic> json) =>
    ImageByBreedEntity(
      status: json['status'] as String?,
      listImagePath:
          (json['message'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ImageByBreedEntityToJson(ImageByBreedEntity instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.listImagePath,
    };
