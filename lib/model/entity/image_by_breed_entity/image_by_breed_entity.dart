import 'package:dog_app/model/entity/image_by_breed_entity/image_breed_custom_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'image_by_breed_entity.g.dart';

@JsonSerializable()
class ImageByBreedEntity {
  @JsonKey(name: 'status')
  final String? status;

  @JsonKey(name: 'message')
  final List<String>? listImagePath;

  ImageByBreedEntity({
    this.status,
    this.listImagePath,
  });

  List<ImageBreedCustomEntity> listImage(String key) {
    List<ImageBreedCustomEntity> list = [];
    for (int i = 0; i < (listImagePath?.length ?? 0); i++) {
      list.add(
          ImageBreedCustomEntity(image: listImagePath?[i] ?? '', key: key));
    }
    return list;
  }

  factory ImageByBreedEntity.fromJson(Map<String, dynamic> json) =>
      _$ImageByBreedEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImageByBreedEntityToJson(this);
}
