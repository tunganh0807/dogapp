import 'dart:core';

import 'package:dio/dio.dart';
import 'package:dog_app/model/entity/breeds_entity/breeds_entity.dart';
import 'package:dog_app/model/entity/image_by_breed_entity/image_by_breed_entity.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi()
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET("/breeds/list/all")
  Future<BreedsEntity> getBreedsEntity();

  @GET("/breed/{breed}/images")
  Future<ImageByBreedEntity> getImageByBreed(
    @Path('breed') String breed,
  );
}
