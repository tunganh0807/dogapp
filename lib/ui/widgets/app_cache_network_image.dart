import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AppCacheNetworkImage extends StatelessWidget {
  final String imageUrl;

  const AppCacheNetworkImage({
    Key? key,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      errorWidget: (context, url, error) => const Icon(Icons.error),
      fit: BoxFit.fill,
      imageUrl: imageUrl,
    );
  }
}
