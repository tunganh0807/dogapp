import 'package:dog_app/common/app_text_style.dart';
import 'package:flutter/material.dart';

class AppToggleButton extends StatelessWidget {
  final List<String> tabItems;
  final TabController tabController;
  final VoidCallback onChangeChecked;
  final bool isChecked;

  const AppToggleButton({
    Key? key,
    required this.isChecked,
    required this.tabController,
    required this.onChangeChecked,
    this.tabItems = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      color: Colors.red,
      padding: const EdgeInsets.all(5),
      child: ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: tabItems.length,
        itemBuilder: (context, index) => buildItem(tabItems[index], isChecked),
        separatorBuilder: (context, index) => const SizedBox(
          width: 10,
        ),
      ),
    );
  }

  // List<bool> listBoolItem() {
  //   List<bool> boolItem = [];
  //   for (int i = 0; i < (tabItems).length; i++) {
  //     boolItem.add(isChecked);
  //   }
  //   return boolItem;
  // }

  Widget buildItem(String title, bool isChecked) {
    return Container(
      color: Colors.blue,
      width: 200,
      child: CheckboxListTile(
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: AppTextStyle.white,
        ),
        value: isChecked,
        onChanged: (bool? value) {
          onChangeChecked.call();
        },
      ),
    );
  }
}
