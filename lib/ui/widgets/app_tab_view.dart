import 'package:cached_network_image/cached_network_image.dart';
import 'package:dog_app/ui/widgets/app_circular_progress_indicator.dart';
import 'package:flutter/material.dart';

class AppTabView extends StatelessWidget {
  final ScrollController scrollController;
  final TabController tabController;
  final List<String> tabItems;
  final List<String?> listImageUrl;
  final int itemCount;
  final VoidCallback onRefresh;

  const AppTabView({
    Key? key,
    required this.scrollController,
    required this.tabController,
    required this.tabItems,
    required this.listImageUrl,
    required this.itemCount,
    required this.onRefresh,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: TabBarView(
        controller: tabController,
        children: buildTabViewItems(),
      ),
    );
  }

  List<Widget> buildTabViewItems() {
    List<Widget> items = [];
    for (int i = 0; i < (tabItems).length; i++) {
      items.add(buildTabViewItem());
    }
    return items;
  }

  Widget buildTabViewItem() {
    return RefreshIndicator(
      onRefresh: () async {
        onRefresh.call();
      },
      child: ListView.separated(
        controller: scrollController,
        itemCount: itemCount + 1,
        itemBuilder: (context, index) {
          if (index == itemCount &&
              itemCount < tabItems.length &&
              tabItems.length > 10) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  AppCircularProgressIndicator(),
                  SizedBox(height: 5),
                  Text('Loading more data'),
                ],
              ),
            );
          }

          if (index > listImageUrl.length) {
            return const SizedBox();
          }

          return _itemBuilder(listImageUrl[index] ?? '');
        },
        separatorBuilder: (context, index) => const SizedBox(height: 15),
      ),
    );
  }

  Widget _itemBuilder(String imageUrl) {
    return CachedNetworkImage(
      errorWidget: (context, url, error) => const Icon(Icons.error),
      fit: BoxFit.fill,
      imageUrl: imageUrl,
    );
  }
}
