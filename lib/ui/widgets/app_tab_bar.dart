import 'package:dog_app/common/app_text_style.dart';
import 'package:flutter/material.dart';

class AppTabBar extends StatelessWidget {
  final List<String> tabItems;
  final TabController tabController;
  final VoidCallback onIndexChanged;

  const AppTabBar({
    Key? key,
    required this.tabController,
    required this.onIndexChanged,
    this.tabItems = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      color: Colors.red,
      padding: const EdgeInsets.all(5),
      child: TabBar(
        isScrollable: true,
        controller: tabController,
        labelColor: Colors.blue,
        tabs: buildTabItems(),
        onTap: (index) {
          onIndexChanged.call();
        },
        indicator: const BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
        ),
        indicatorWeight: 1,
      ),
    );
  }

  List<Widget> buildTabItems() {
    List<Widget> items = [];
    for (int i = 0; i < (tabItems).length; i++) {
      items.add(
        buildTabItem(tabItems[i]),
      );
    }
    return items;
  }

  Widget buildTabItem(String title) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: AppTextStyle.white,
    );
  }
}
