import 'package:dog_app/model/entity/image_by_breed_entity/image_breed_custom_entity.dart';
import 'package:dog_app/ui/widgets/app_cache_network_image.dart';
import 'package:dog_app/ui/widgets/app_circular_progress_indicator.dart';
import 'package:flutter/material.dart';

class ListImageWidget extends StatelessWidget {
  final int itemCount;
  final bool loadingMore;
  final List<ImageBreedCustomEntity> listImage;
  final VoidCallback onRefresh;
  final ScrollController scrollController;

  const ListImageWidget({
    Key? key,
    required this.itemCount,
    required this.listImage,
    required this.onRefresh,
    required this.scrollController,
    required this.loadingMore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        onRefresh.call();
      },
      child: ListView.separated(
        controller: scrollController,
        itemCount: itemCount + 1,
        itemBuilder: (context, index) {
          if (index == itemCount) {
            return Visibility(
              visible: loadingMore,
              child: Center(
                child: Column(
                  children: const [
                    SizedBox(height: 10),
                    AppCircularProgressIndicator(),
                    SizedBox(height: 5),
                    Text(
                      'Loading more data',
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          }

          return AppCacheNetworkImage(
            imageUrl: listImage[index].image,
          );
        },
        separatorBuilder: (context, index) => const SizedBox(height: 5),
      ),
    );
  }
}
