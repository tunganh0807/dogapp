import 'package:dog_app/model/entity/breeds_entity/breeds_custom_entity.dart';
import 'package:flutter/material.dart';

class ScrollBarWidget extends StatelessWidget {
  final List<BreedsCustomEntity> listBreed;
  final ValueChanged onTapItem;

  const ScrollBarWidget({
    Key? key,
    required this.listBreed,
    required this.onTapItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      itemCount: listBreed.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return _itemScrollBar(
          breed: listBreed[index].breed,
          isSelect: listBreed[index].isSelect,
          index: index,
        );
      },
      separatorBuilder: (context, index) {
        return const SizedBox(width: 5);
      },
    );
  }

  Widget _itemScrollBar({
    required String breed,
    required bool isSelect,
    required int index,
  }) {
    return InkWell(
      onTap: () {
        onTapItem.call(index);
        // _homeCubit.onTapBreed(index);
        // _homeCubit.onChangeTabOrRefresh();
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: isSelect ? Colors.blue : Colors.white,
            border: Border.all(color: Colors.red)),
        padding: const EdgeInsets.all(10),
        child: Text(
          breed,
          style: TextStyle(
            color: isSelect ? Colors.white : Colors.red,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
