part of 'home_cubit.dart';

class HomeState extends Equatable {
  final LoadStatus? loadImageDataStatus;
  final LoadStatus? loadBreedDataStatus;
  final LoadStatus? changeStatusBreed;
  final LoadStatus? refreshStatus;
  final List<BreedsCustomEntity> listBreed;
  final ImageByBreedEntity? imageByBreedEntity;
  final List<ImageBreedCustomEntity> listImage;
  final int itemCount;
  final bool canLoadMore;
  final LoadStatus selectBreed;
  final LoadStatus unSelectBreed;

  const HomeState({
    this.loadImageDataStatus = LoadStatus.initial,
    this.loadBreedDataStatus = LoadStatus.initial,
    this.changeStatusBreed = LoadStatus.initial,
    this.refreshStatus = LoadStatus.initial,
    this.selectBreed = LoadStatus.initial,
    this.unSelectBreed = LoadStatus.initial,
    this.listImage = const [],
    this.listBreed = const [],
    this.imageByBreedEntity,
    this.itemCount = 10,
    this.canLoadMore = true,
  });

  @override
  List<Object?> get props => [
        loadImageDataStatus,
        loadBreedDataStatus,
        changeStatusBreed,
        refreshStatus,
        selectBreed,
        unSelectBreed,
        listImage,
        listBreed,
        imageByBreedEntity,
        itemCount,
        canLoadMore,
      ];

  HomeState copyWith({
    LoadStatus? loadImageDataStatus,
    LoadStatus? loadBreedDataStatus,
    LoadStatus? changeStatusBreed,
    LoadStatus? refreshStatus,
    List<BreedsCustomEntity>? listBreed,
    ImageByBreedEntity? imageByBreedEntity,
    List<ImageBreedCustomEntity>? listImage,
    int? itemCount,
    bool? canLoadMore,
    LoadStatus? selectBreed,
    LoadStatus? unSelectBreed,
  }) {
    return HomeState(
      loadImageDataStatus: loadImageDataStatus ?? this.loadImageDataStatus,
      loadBreedDataStatus: loadBreedDataStatus ?? this.loadBreedDataStatus,
      changeStatusBreed: changeStatusBreed ?? this.changeStatusBreed,
      refreshStatus: refreshStatus ?? this.refreshStatus,
      listBreed: listBreed ?? this.listBreed,
      imageByBreedEntity: imageByBreedEntity ?? this.imageByBreedEntity,
      listImage: listImage ?? this.listImage,
      itemCount: itemCount ?? this.itemCount,
      canLoadMore: canLoadMore ?? this.canLoadMore,
      selectBreed: selectBreed ?? this.selectBreed,
      unSelectBreed: unSelectBreed ?? this.unSelectBreed,
    );
  }
}
