import 'package:dog_app/common/app_text_style.dart';
import 'package:dog_app/model/enum/load_status.dart';
import 'package:dog_app/repository/breed_repository.dart';
import 'package:dog_app/repository/image_repository.dart';
import 'package:dog_app/ui/home/home_cubit.dart';
import 'package:dog_app/ui/home/widgets/list_image_widget.dart';
import 'package:dog_app/ui/home/widgets/scroll_bar_widget.dart';
import 'package:dog_app/ui/widgets/app_circular_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        final imagesRepo = RepositoryProvider.of<ImageRepository>(context);
        final breedRepo = RepositoryProvider.of<BreedRepository>(context);
        return HomeCubit(
          breedRepository: breedRepo,
          imageRepository: imagesRepo,
        );
      },
      child: const HomeChildPage(),
    );
  }
}

class HomeChildPage extends StatefulWidget {
  const HomeChildPage({Key? key}) : super(key: key);

  @override
  State<HomeChildPage> createState() => _HomeChildPageState();
}

class _HomeChildPageState extends State<HomeChildPage>
    with TickerProviderStateMixin {
  late ScrollController _scrollController;
  late HomeCubit _homeCubit;
  late int indexSelect;

  @override
  void initState() {
    super.initState();
    _homeCubit = HomeCubit(
      breedRepository: RepositoryProvider.of<BreedRepository>(context),
      imageRepository: RepositoryProvider.of<ImageRepository>(context),
    );
    _scrollController = ScrollController();
    _homeCubit.initListBreed();
    _scrollController.addListener(() {
      _loadMoreMessage();
    });
  }

  void _loadMoreMessage() {
    if (_scrollController.position.pixels >=
        _scrollController.position.maxScrollExtent) {
      _homeCubit.updateLoadMoreStatus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _buildBodyWidget(),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _homeCubit,
      buildWhen: (previous, current) =>
          previous.loadBreedDataStatus != current.loadBreedDataStatus,
      builder: (context, state) {
        switch (state.loadBreedDataStatus) {
          case LoadStatus.loading:
            return const Center(
              child: AppCircularProgressIndicator(),
            );

          case LoadStatus.fail:
            return Center(
              child: Text(
                'Error !',
                style: AppTextStyle.blackS32,
              ),
            );

          default:
            return Column(
              children: [
                _scrollBar(),
                const SizedBox(height: 20),
                Expanded(
                  child: _listImage(),
                ),
              ],
            );
        }
      },
    );
  }

  Widget _listImage() {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _homeCubit,
      buildWhen: (previous, current) =>
          previous.loadImageDataStatus != current.loadImageDataStatus ||
          previous.itemCount != current.itemCount,
      builder: (context, state) {
        switch (state.loadImageDataStatus) {
          case LoadStatus.loading:
            return const Center(child: AppCircularProgressIndicator());
          case LoadStatus.fail:
            return Text(
              "Can't load image",
              style: AppTextStyle.blackS20,
            );
          default:
            if (state.listImage.isEmpty) {
              return Center(
                child: Text(
                  'No data',
                  style: AppTextStyle.blackS20,
                ),
              );
            } else {
              return ListImageWidget(
                loadingMore:
                    state.loadImageDataStatus == LoadStatus.loadingMore,
                itemCount: state.itemCount,
                onRefresh: () {
                  _homeCubit.onRefresh();
                },
                scrollController: _scrollController,
                listImage: state.listImage,
              );
            }
        }
      },
    );
  }

  Widget _scrollBar() {
    return Container(
      color: Colors.white,
      height: 40,
      child: BlocBuilder<HomeCubit, HomeState>(
        bloc: _homeCubit,
        buildWhen: (previous, current) =>
            previous.changeStatusBreed != current.changeStatusBreed,
        builder: (context, state) {
          return ScrollBarWidget(
            listBreed: state.listBreed,
            onTapItem: (index) {
              _homeCubit.onTapBreed(index);
              _homeCubit.onRefresh();
              indexSelect = index;
            },
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.removeListener(
      () {
        _loadMoreMessage();
      },
    );
    _homeCubit.close();
    super.dispose();
  }
}
