import 'package:dog_app/model/entity/breeds_entity/breeds_custom_entity.dart';
import 'package:dog_app/model/entity/image_by_breed_entity/image_breed_custom_entity.dart';
import 'package:dog_app/model/entity/image_by_breed_entity/image_by_breed_entity.dart';
import 'package:dog_app/model/enum/load_status.dart';
import 'package:dog_app/repository/breed_repository.dart';
import 'package:dog_app/repository/image_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  ImageRepository imageRepository;
  BreedRepository breedRepository;

  HomeCubit({
    required this.imageRepository,
    required this.breedRepository,
  }) : super(const HomeState());

  Future<void> updateLoadMoreStatus() async {
    emit(state.copyWith(loadImageDataStatus: LoadStatus.loadingMore));
    await Future.delayed(const Duration(seconds: 3));
    emit(
      state.copyWith(
        loadImageDataStatus: LoadStatus.success,
        itemCount: state.itemCount + 10 < state.listImage.length
            ? state.itemCount + 10
            : state.listImage.length,
      ),
    );
  }

  Future<void> initListBreed() async {
    try {
      emit(state.copyWith(loadBreedDataStatus: LoadStatus.loading));
      List<BreedsCustomEntity> listBreed = [];
      final breedResult = await breedRepository.getBreedEntity();
      if (breedResult == null) {
        emit(state.copyWith(loadBreedDataStatus: LoadStatus.fail));
      } else {
        listBreed = breedResult.getListBreed();
        listBreed.first.isSelect = true;
        emit(
          state.copyWith(
            listBreed: listBreed,
            loadBreedDataStatus: LoadStatus.success,
          ),
        );
        if (listBreed.isNotEmpty) {
          getImagesByBreed(listBreed[0].breed, 0);
        }
      }
    } catch (e) {
      emit(state.copyWith(loadBreedDataStatus: LoadStatus.fail));
      debugPrint(e.toString());
    }
  }

  Future<void> onRefresh() async {
    List<BreedsCustomEntity> list = List.from(state.listBreed);
    List<ImageBreedCustomEntity> listImage = [];
    for (int i = 0; i < list.length; i++) {
      if (list[i].isSelect == true) {
        await getImagesByBreed(list[i].breed, i);
        listImage.addAll(
          state.imageByBreedEntity?.listImage(state.listBreed[i].breed) ?? [],
        );
      }
    }
    emit(state.copyWith(
      listImage: listImage,
      itemCount: listImage.length < 10 ? listImage.length : 10,
    ));
  }

  void onTapBreed(int index) async {
    try {
      emit(state.copyWith(
        changeStatusBreed: LoadStatus.loading,
        loadImageDataStatus: LoadStatus.loading,
      ));
      List<BreedsCustomEntity> list = List.from(state.listBreed);
      list[index].isSelect = !list[index].isSelect;
      emit(state.copyWith(listBreed: list));
      List<ImageBreedCustomEntity> listImage = List.from(state.listImage);
      if (list[index].isSelect == true) {
        await getImagesByBreed(state.listBreed[index].breed, index);
        listImage.addAll(
          state.imageByBreedEntity?.listImage(state.listBreed[index].breed) ??
              [],
        );
      } else {
        listImage.removeWhere(
            (element) => element.key == state.listBreed[index].breed);
      }
      emit(
        state.copyWith(
          listImage: listImage,
          itemCount: listImage.length > 10 ? 10 : listImage.length,
          loadImageDataStatus: LoadStatus.success,
          changeStatusBreed: LoadStatus.success,
        ),
      );
    } catch (e) {
      debugPrint(e.toString());
      emit(state.copyWith(changeStatusBreed: LoadStatus.fail));
    }
  }

  Future<void> getImagesByBreed(String breed, int index) async {
    emit(
      state.copyWith(
        loadImageDataStatus: LoadStatus.loading,
      ),
    );
    try {
      final imagesResult = await imageRepository.getImageEntity(breed: breed);
      List<ImageBreedCustomEntity>? listImage = List.from(state.listImage);
      listImage
          .addAll(imagesResult?.listImage(state.listBreed[index].breed) ?? []);
      emit(
        state.copyWith(
          listImage: listImage,
          itemCount: listImage.length > 10 ? 10 : listImage.length,
          imageByBreedEntity: imagesResult,
          loadImageDataStatus: LoadStatus.success,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          loadImageDataStatus: LoadStatus.fail,
        ),
      );
      debugPrint(e.toString());
    }
  }
}
